//
//  MVPPresenterProtocol.swift
//  Architecture
//
//  Created by alex borisoft on 03.11.2021.
//

import Foundation


protocol MVPPresenterProtocol {
    
    var view: MVPViewable { get set }
    
    func increase()
    
    func deacrease()
    
    func clean()
    
}
