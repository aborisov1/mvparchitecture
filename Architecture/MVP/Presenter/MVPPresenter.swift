//
//  MVPPresenter.swift
//  Architecture
//
//  Created by alex borisoft on 03.11.2021.
//

import Foundation

final class MVPPresenter: MVPPresenterProtocol {

    unowned var view: MVPViewable
    
    private let service: CounterService = .init()
    
    private var didSetup: Bool = false
    
    private var counter: Int = 0 {
        didSet {
           updatePresentationState()
            
            guard didSetup else {
                return
            }
            
            service.saveCounter(counter)
        }
    }
    
    init(view: MVPViewable) {
        self.view = view
        setupIntialState()
    }
    
    private func setupIntialState() {
        counter = service.fetchCounter()
        didSetup = true
    }
    
    func increase() {
        counter += 1
    }
    
    func deacrease() {
        counter -= 1
    }
    
    func clean() {
        counter = 0
    }
    
    private func needUpdateCounterLabel() {
        view.setCountLabel("Значение \(counter)")
    }
    
    private func needUpdateIsEnabledCleanButton() {
        view.setEnabledCleaningButton(counter != 0)
    }
    
    private func updatePresentationState() {
        needUpdateCounterLabel()
        needUpdateIsEnabledCleanButton()
    }

}
