//
//  MVPViewController.swift
//  Architecture
//
//  Created by alex borisoft on 03.11.2021.
//

import Foundation
import UIKit


final class MVPViewController: UIViewController, MVPViewable {
    
    // MARK: - Properties
    
    private var presenter: MVPPresenterProtocol!
    
    // MARK: - IBOutlets
   
    @IBOutlet weak var countLabel: UILabel!
    
    @IBOutlet weak var cleanButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = MVPPresenter(view: self)
    }

    // MARK: - IBActions
    
    @IBAction func onDecreaseButtonTap(_ sender: Any) {
        presenter.deacrease()
    }
    
    @IBAction func onIncreaseButtonTap(_ sender: Any) {
        presenter.increase()
    }
    
    @IBAction func onCleanButtonTap(_ sender: Any) {
        presenter.clean()
    }
    
    // MARK: - MVPViewable
    
    func setCountLabel(_ value: String) {
        countLabel.text = value
    }
    
    func setEnabledCleaningButton(_ isEnabled: Bool) {
        cleanButton.isEnabled = isEnabled
    }
}
