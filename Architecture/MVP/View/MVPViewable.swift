//
//  MVPViewable.swift
//  Architecture
//
//  Created by alex borisoft on 03.11.2021.
//

import Foundation

protocol MVPViewable: AnyObject {
    
    func setCountLabel(_ value: String)
    
    func setEnabledCleaningButton(_ isEnabled: Bool)
}
