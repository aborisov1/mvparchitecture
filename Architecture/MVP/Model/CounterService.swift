//
//  CounterService.swift
//  Architecture
//
//  Created by alex borisoft on 03.11.2021.
//

import Foundation

final class CounterService {
    
    private let key: String = "CounterKey"
    
    func fetchCounter() -> Int {
        guard let counter = UserDefaults.standard.value(forKey: key) as? Int else {
            return 0
        }
        return counter
    }
    
    func saveCounter(_ value: Int) {
        UserDefaults.standard.set(value, forKey: key)
    }
}
